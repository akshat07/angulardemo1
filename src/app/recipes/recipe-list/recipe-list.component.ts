import { Component, OnInit } from '@angular/core';
import {Recipe} from '../recipe.model'
@Component({
  selector: 'app-recipe-list',
  templateUrl: './recipe-list.component.html',
  styleUrls: ['./recipe-list.component.css']
})
export class RecipeListComponent implements OnInit {
recipes: Recipe[]= [
  new Recipe('A Test Recipe', 'This is simply a test','https://www.google.com/imgres?imgurl=https%3A%2F%2Fthumbs.dreamstime.com%2Fb%2Frecipe-word-text-green-leaf-logo-icon-design-white-background-suitable-card-typography-143638205.jpg&imgrefurl=https%3A%2F%2Fwww.dreamstime.com%2Frecipe-word-text-green-leaf-logo-icon-design-white-background-suitable-card-typography-image143638205&tbnid=KmmfD59e5C_PFM&vet=12ahUKEwixlpH_-8bvAhWQeH0KHXgvCtEQMygAegUIARCQAQ..i&docid=0uYCjKRiqhnfEM&w=800&h=514&q=recipe%20logo.jpg&ved=2ahUKEwixlpH_-8bvAhWQeH0KHXgvCtEQMygAegUIARCQAQ')
];
  constructor() { }

  ngOnInit(): void {
  }

}
